package com.dinsaren.webdeliverymanagement.service.impl;

import com.dinsaren.webdeliverymanagement.constants.Constant;
import com.dinsaren.webdeliverymanagement.models.Category;
import com.dinsaren.webdeliverymanagement.repository.CategoryRepository;
import com.dinsaren.webdeliverymanagement.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository  categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> findAll() {
        List<String> listStatus = new ArrayList<>();
        listStatus.add(Constant.STATUS_DISABLE);
        listStatus.add(Constant.STATUS_DELETE);
        listStatus.add(Constant.STATUS_ACTIVE);
        return categoryRepository.findAllByStatusIn(listStatus);
    }

    @Override
    public List<Category> findAllStatusActive() {
        return categoryRepository.findAllByStatus(Constant.STATUS_ACTIVE);
    }

    @Override
    public void save(Category category) {
        category.setStatus(Constant.STATUS_ACTIVE);
        categoryRepository.save(category);
    }

    @Override
    public void update(Category category) {
        var find = categoryRepository.findById(category.getId());
        if(find.isPresent()){
            categoryRepository.save(category);
        }
    }

    @Override
    public void delete(Category category) {
        var find = categoryRepository.findById(category.getId());
        if(find.isPresent()){
            category.setStatus(Constant.STATUS_DELETE);
            categoryRepository.save(category);
        }
    }

    @Override
    public Category findById(Integer id) {
       var category = categoryRepository.findById(Long.valueOf(id));
       if(category.isPresent()){
           return category.get();
       }
       return null;
    }
}
