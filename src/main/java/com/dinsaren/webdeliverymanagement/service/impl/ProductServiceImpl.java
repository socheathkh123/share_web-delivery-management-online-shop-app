package com.dinsaren.webdeliverymanagement.service.impl;

import com.dinsaren.webdeliverymanagement.constants.Constant;
import com.dinsaren.webdeliverymanagement.models.Product;
import com.dinsaren.webdeliverymanagement.payload.req.ProductCreateReq;
import com.dinsaren.webdeliverymanagement.payload.res.ProductRes;
import com.dinsaren.webdeliverymanagement.repository.ProductRepository;
import com.dinsaren.webdeliverymanagement.service.CategoryService;
import com.dinsaren.webdeliverymanagement.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final CategoryService categoryService;

    public ProductServiceImpl(ProductRepository productRepository, CategoryService categoryService) {
        this.productRepository = productRepository;
        this.categoryService = categoryService;
    }

    @Override
    public void create(ProductCreateReq req) {
        Product product = new Product();
        product.setName(req.getName());
        product.setCreatedAt(new Date());
        product.setCreatedBy(req.getCreatedBy());
        product.setDescription(req.getDescription());
        product.setPrice(req.getPrice());
        product.setQty(req.getQty());
        product.setImage(req.getImage());
        product.setDiscount(req.getDiscount());
        product.setStatus(Constant.STATUS_ACTIVE);
        product.setCategoryId(req.getCategoryId());
        productRepository.save(product);
    }

    @Override
    public List<ProductRes> findProductByShopIdAndStatus(Integer shopId) {
        List<ProductRes> resLs = new ArrayList<>();
        var resList = productRepository.findAllByStatusAndCreatedBy(Constant.STATUS_ACTIVE, shopId);
        resList.forEach(c->{
            ProductRes res = new ProductRes();
            res.setName(c.getName());
            res.setCreatedBy(c.getCreatedBy());
            res.setDescription(c.getDescription());
            res.setPrice(c.getPrice());
            res.setQty(c.getQty());
            res.setImage(c.getImage());
            res.setDiscount(c.getDiscount());
            res.setStatus(c.getStatus());
            res.setCategoryId(c.getCategoryId());
            var category = categoryService.findById(c.getCategoryId());
            if(null != category){
                res.setCategoryName(category.getName());
            }
            resLs.add(res);
        });
        return resLs;
    }
}
