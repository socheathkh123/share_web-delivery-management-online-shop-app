package com.dinsaren.webdeliverymanagement.service;

import com.dinsaren.webdeliverymanagement.payload.req.ProductCreateReq;
import com.dinsaren.webdeliverymanagement.payload.res.ProductRes;

import java.util.List;

public interface ProductService {
    void create(ProductCreateReq req);
    List<ProductRes> findProductByShopIdAndStatus(Integer shopId);
}
