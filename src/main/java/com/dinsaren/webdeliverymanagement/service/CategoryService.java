package com.dinsaren.webdeliverymanagement.service;

import com.dinsaren.webdeliverymanagement.models.Category;

import java.util.List;

public interface CategoryService {
    List<Category> findAll();
    List<Category> findAllStatusActive();
    void save(Category category);
    void update(Category category);
    void delete(Category category);
    Category findById(Integer id);
}
