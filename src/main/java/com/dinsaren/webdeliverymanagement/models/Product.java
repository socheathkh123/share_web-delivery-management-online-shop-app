package com.dinsaren.webdeliverymanagement.models;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Table(name = "product")
@Entity
@Data
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "image")
    private String image;
    @Column(name = "description")
    private String description;
    @Column(name = "qty")
    private int qty;
    @Column(name = "price")
    private double price;
    @Column(name = "discount")
    private String discount;
    @Column(name = "created_by")
    private int createdBy;
    @Column(name = "created_at")
    private Date createdAt;
    private String status;
    @Column(name = "category_id")
    private int categoryId;

}
