package com.dinsaren.webdeliverymanagement.models;

import lombok.Data;

import javax.persistence.*;

@Table(name = "user_device_token")
@Entity
@Data
public class UserDeviceToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "user_id")
    private int userId;
    @Column(name = "device_token")
    private String deviceToken;
    private String status;
}
