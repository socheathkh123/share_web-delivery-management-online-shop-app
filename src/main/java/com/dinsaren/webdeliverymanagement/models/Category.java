package com.dinsaren.webdeliverymanagement.models;

import lombok.Data;

import javax.persistence.*;

@Table(name = "category")
@Entity
@Data
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    private String status;
}
