package com.dinsaren.webdeliverymanagement.controllers;

import com.dinsaren.webdeliverymanagement.payload.res.MessageGeneralRes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/user")
@PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN') or hasRole('USER')")
public class UserController {
    private MessageGeneralRes messageRes;
    @GetMapping
    public ResponseEntity<Object> getAll() {
        try {
            messageRes = new MessageGeneralRes();
            messageRes.getSuccess("User Can get data");
            return new ResponseEntity<>(messageRes, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
        }

    }

}
