package com.dinsaren.webdeliverymanagement.controllers.shop;

import com.dinsaren.webdeliverymanagement.payload.req.ProductCreateReq;
import com.dinsaren.webdeliverymanagement.payload.res.MessageGeneralRes;
import com.dinsaren.webdeliverymanagement.service.CategoryService;
import com.dinsaren.webdeliverymanagement.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/shop")
@PreAuthorize("hasRole('SHOP') or hasRole('ADMIN')")
public class ShopController {
    private final CategoryService categoryService;
    private final ProductService productService;
    private MessageGeneralRes messageRes;
    public ShopController(CategoryService categoryService, ProductService productService) {
        this.categoryService = categoryService;
        this.productService = productService;
    }

    @GetMapping("/categories")
    public ResponseEntity<Object> findAllCategory(){
        try {
            var list = categoryService.findAllStatusActive();
            messageRes = new MessageGeneralRes();
            messageRes.getSuccess(list);
            return new ResponseEntity<>(messageRes, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
        }
    }

    @PostMapping("/product/create")
    public ResponseEntity<Object> createProduct(@RequestBody ProductCreateReq req){
        try {
            productService.create(req);
            messageRes = new MessageGeneralRes();
            messageRes.createSuccess();
            return new ResponseEntity<>(messageRes, HttpStatus.OK);
        } catch (Exception e) {
            messageRes.badGateway();
            return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
        }
    }

    @GetMapping("/product/{shop}")
    public ResponseEntity<Object> findProductByShopId(@PathVariable("shop") Integer shopId){
        try {
            var list = productService.findProductByShopIdAndStatus(shopId);
            messageRes = new MessageGeneralRes();
            messageRes.getSuccess(list);
            return new ResponseEntity<>(messageRes, HttpStatus.OK);
        } catch (Exception e) {
            messageRes.badGateway();
            return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
        }
    }

}
