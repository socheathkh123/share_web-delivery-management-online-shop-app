package com.dinsaren.webdeliverymanagement.controllers.admin;

import com.dinsaren.webdeliverymanagement.models.Category;
import com.dinsaren.webdeliverymanagement.payload.res.MessageGeneralRes;
import com.dinsaren.webdeliverymanagement.service.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/admin/category")
@PreAuthorize("hasRole('ADMIN')")
public class AdminCategoryController {
    private final CategoryService categoryService;
    private MessageGeneralRes messageRes;

    public AdminCategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping
    public ResponseEntity<Object> getAll() {
        try {
            var list = categoryService.findAll();
            messageRes = new MessageGeneralRes();
            messageRes.getSuccess(list);
            return new ResponseEntity<>(messageRes, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
        }

    }

    @PostMapping("/create")
    public ResponseEntity<Object> create(@RequestBody Category req) {
        try {
            categoryService.save(req);
            messageRes = new MessageGeneralRes();
            messageRes.createSuccess();
            return new ResponseEntity<>(messageRes, HttpStatus.OK);
        } catch (Exception e) {
            messageRes.badGateway();
            return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
        }

    }

    @PostMapping("/update")
    public ResponseEntity<Object> update(@RequestBody Category req) {
        try {
            categoryService.save(req);
            messageRes = new MessageGeneralRes();
            messageRes.updateSuccess();
            return new ResponseEntity<>(messageRes, HttpStatus.OK);

        } catch (Exception e) {
            messageRes.badGateway();
            return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
        }

    }

    @PostMapping("/delete")
    public ResponseEntity<Object> find(@RequestBody Category req) {
        try {
            categoryService.delete(req);
            messageRes = new MessageGeneralRes();
            messageRes.deleteSuccess();
            return new ResponseEntity<>(messageRes, HttpStatus.OK);

        } catch (Exception e) {
            messageRes.badGateway();
            return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> findById(@PathVariable("id") Integer id){
        try {
            var category = categoryService.findById(id);
            messageRes = new MessageGeneralRes();
            messageRes.deleteSuccess();
            return new ResponseEntity<>(category, HttpStatus.OK);
        }catch (Exception e){
            messageRes.badGateway();
            return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
        }
    }

}
