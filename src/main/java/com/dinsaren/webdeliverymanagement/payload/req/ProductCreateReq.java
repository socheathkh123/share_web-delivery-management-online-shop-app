package com.dinsaren.webdeliverymanagement.payload.req;

import lombok.Data;

@Data
public class ProductCreateReq {
    private Long id;
    private String name;
    private String image;
    private String description;
    private int qty;
    private double price;
    private String discount;
    private int createdBy;
    private String status;
    private int categoryId;
}
