package com.dinsaren.webdeliverymanagement.payload.req;

import lombok.Data;

import java.util.Set;

@Data
public class SignupReq {
  private String username;
  private String email;
  private String password;

}
