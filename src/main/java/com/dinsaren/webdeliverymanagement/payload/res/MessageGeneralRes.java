package com.dinsaren.webdeliverymanagement.payload.res;

import lombok.Data;

@Data
public class MessageGeneralRes {
  private String message;
  private String errorMessage;
  private Object data;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public void setSuccess(String message, String errorMessage, Object data){
    this.setData(data);
    this.setErrorMessage(errorMessage);
    this.setMessage(message);
  }

  public void getSuccess(Object data){
    this.setData(data);
    this.setErrorMessage("SUC-001");
    this.setMessage("Gat Data Successfully");
  }

  public void createSuccess(){
    this.setData(null);
    this.setErrorMessage("ERR-001");
    this.setMessage("Create Data Successfully");
  }

  public void updateSuccess(){
    this.setData(null);
    this.setErrorMessage("ERR-002");
    this.setMessage("Update Data Successfully");
  }

  public void deleteSuccess(){
    this.setData(null);
    this.setErrorMessage("ERR-004");
    this.setMessage("Delete Data Successfully");
  }

  public void dataNotFound(){
    this.setData(null);
    this.setErrorMessage("ERR-005");
    this.setMessage("Delete Data Successfully");
  }

  public void badGateway(){
    this.setData(null);
    this.setErrorMessage("ERR-007");
    this.setMessage("Bad Gateway");
  }

  public void badRequest(){
    this.setData(null);
    this.setErrorMessage("ERR-008");
    this.setMessage("Bad Request");
  }

}
