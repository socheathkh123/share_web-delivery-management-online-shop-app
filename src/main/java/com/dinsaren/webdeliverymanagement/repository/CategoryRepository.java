package com.dinsaren.webdeliverymanagement.repository;

import com.dinsaren.webdeliverymanagement.models.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {
    List<Category> findAllByStatus(String status);
    List<Category> findAllByStatusIn(List<String> status);
}
