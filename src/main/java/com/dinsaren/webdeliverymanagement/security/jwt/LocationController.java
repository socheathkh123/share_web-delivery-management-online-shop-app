//package com.dinsaren.webdeliverymanagement.controllers.admin;
//
//import com.dinsaren.webdeliverymanagement.payload.res.MessageGeneralRes;
//import com.dinsaren.webdeliverymanagement.security.services.LocationService;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.*;
//
//@CrossOrigin(origins = "*", maxAge = 3600)
//@RestController
//@RequestMapping("/api/admin/location")
//@PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
//public class LocationController {
//    private MessageGeneralRes messageRes;
//    private final LocationService locationService;
//
//    public LocationController(LocationService locationService) {
//        this.locationService = locationService;
//    }
//
//    @GetMapping
//    public ResponseEntity<Object> getAll() {
//        try {
//            var list = locationService.findAll();
//            messageRes = new MessageGeneralRes();
//            messageRes.getSuccess(list);
//            return new ResponseEntity<>(messageRes, HttpStatus.OK);
//        } catch (Exception e) {
//            return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
//        }
//
//    }
//
//    @PostMapping("/create")
//    public ResponseEntity<Object> create(@RequestBody Location req) {
//        try {
//            locationService.save(req);
//            messageRes = new MessageGeneralRes();
//            messageRes.createSuccess();
//            return new ResponseEntity<>(messageRes, HttpStatus.OK);
//        } catch (Exception e) {
//            messageRes.badGateway();
//            return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
//        }
//
//    }
//
//    @PostMapping("/update")
//    public ResponseEntity<Object> update(@RequestBody Location req) {
//        try {
//            locationService.save(req);
//            messageRes = new MessageGeneralRes();
//            messageRes.updateSuccess();
//            return new ResponseEntity<>(messageRes, HttpStatus.OK);
//
//        } catch (Exception e) {
//            messageRes.badGateway();
//            return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
//        }
//
//    }
//
//    @GetMapping("/{id}")
//    public ResponseEntity<Object> find(@PathVariable("id") Integer id) {
//        try {
//            var location = locationService.findById(id);
//            messageRes = new MessageGeneralRes();
//            messageRes.getSuccess(location);
//            return new ResponseEntity<>(messageRes, HttpStatus.OK);
//
//        } catch (Exception e) {
//            messageRes.badGateway();
//            return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
//        }
//
//    }
//
//    @PostMapping("/delete")
//    public ResponseEntity<Object> find(@RequestBody Location req) {
//        try {
//            locationService.delete(req);
//            messageRes = new MessageGeneralRes();
//            messageRes.deleteSuccess();
//            return new ResponseEntity<>(messageRes, HttpStatus.OK);
//
//        } catch (Exception e) {
//            messageRes.badGateway();
//            return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
//        }
//
//    }
//
//}
