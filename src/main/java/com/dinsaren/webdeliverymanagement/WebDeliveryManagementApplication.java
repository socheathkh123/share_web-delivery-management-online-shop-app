package com.dinsaren.webdeliverymanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebDeliveryManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebDeliveryManagementApplication.class, args);
    }

}
